class CreateSpecifications < ActiveRecord::Migration[5.2]
  def change
    create_table :specifications do |t|
      t.string :field
      t.text :content

      t.timestamps
    end
  end
end
