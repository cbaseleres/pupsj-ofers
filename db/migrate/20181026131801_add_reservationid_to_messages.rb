class AddReservationidToMessages < ActiveRecord::Migration[5.2]
  def change
    add_column :messages, :reservation_id, :integer
    add_column :messages, :read, :boolean, default: false
  end
end
