class AddFacilityIdToSpecifications < ActiveRecord::Migration[5.2]
  def change
    add_reference :specifications, :facility, index: true
  end
end
