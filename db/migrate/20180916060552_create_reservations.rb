class CreateReservations < ActiveRecord::Migration[5.2]
  def change
    create_table :reservations do |t|
      t.date :dateFrom
      t.date :dateTo
      t.time :timeFrom
      t.time :timeTo
      t.string :purpose
      t.string :status
      t.string :reference
      t.string :claimed_at
      t.string :returned_at
      t.string :sub_purpose
      t.references :user
      t.timestamps
    end
  end
end
