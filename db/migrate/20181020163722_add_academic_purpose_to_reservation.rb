class AddAcademicPurposeToReservation < ActiveRecord::Migration[5.2]
  def change
    add_column :reservations, :academicPurpose, :string
  end
end
