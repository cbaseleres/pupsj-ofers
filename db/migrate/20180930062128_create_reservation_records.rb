class CreateReservationRecords < ActiveRecord::Migration[5.2]
  def change
    create_table :reservation_records do |t|
      t.references :reservation, foreign_key: true
      t.references :facility, foreign_key: true
      t.references :item, foreign_key: true
      t.integer :quantity
      t.timestamps
    end
  end
end
