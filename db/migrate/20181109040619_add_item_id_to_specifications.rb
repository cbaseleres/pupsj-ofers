class AddItemIdToSpecifications < ActiveRecord::Migration[5.2]
  def change
    add_reference :specifications, :item, index: true
  end
end
