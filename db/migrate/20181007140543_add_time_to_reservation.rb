class AddTimeToReservation < ActiveRecord::Migration[5.2]
  def change
    add_column :reservations, :claim_time, :datetime
    add_column :reservations, :return_time, :datetime
    add_column :reservations, :remarks, :string
  end
end
