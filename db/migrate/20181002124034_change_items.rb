class ChangeItems < ActiveRecord::Migration[5.2]
  def change
    remove_column :items, :category, :string
    remove_column :items, :propertyNumber, :string
    add_column :items, :quantity, :integer
  end
end
