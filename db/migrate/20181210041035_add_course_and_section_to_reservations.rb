class AddCourseAndSectionToReservations < ActiveRecord::Migration[5.2]
  def change
    add_column :reservations, :course_and_section, :string
  end
end
