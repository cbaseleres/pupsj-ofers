class ChangeBlockInUser < ActiveRecord::Migration[5.2]
  def change
    change_column(:users, :block, :boolean, :default => false)
  end
end
