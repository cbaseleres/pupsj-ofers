class AddFacultyToOrganization < ActiveRecord::Migration[5.2]
  def change
    add_column :organizations, :facultyInCharge, :string
  end
end
