# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_12_10_041035) do

  create_table "courses", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "facilities", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "field_of_studies", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "items", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "quantity"
    t.text "description"
  end

  create_table "messages", force: :cascade do |t|
    t.string "title"
    t.text "content"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "reservation_id"
    t.boolean "read", default: false
    t.index ["user_id"], name: "index_messages_on_user_id"
  end

  create_table "organizations", force: :cascade do |t|
    t.string "name"
    t.string "course"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "facultyInCharge"
  end

  create_table "professors", force: :cascade do |t|
    t.string "name"
    t.string "fieldOfStudy"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email"
  end

  create_table "reservation_records", force: :cascade do |t|
    t.integer "reservation_id"
    t.integer "facility_id"
    t.integer "item_id"
    t.integer "quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["facility_id"], name: "index_reservation_records_on_facility_id"
    t.index ["item_id"], name: "index_reservation_records_on_item_id"
    t.index ["reservation_id"], name: "index_reservation_records_on_reservation_id"
  end

  create_table "reservations", force: :cascade do |t|
    t.date "dateFrom"
    t.date "dateTo"
    t.time "timeFrom"
    t.time "timeTo"
    t.string "purpose"
    t.string "status", default: "Active"
    t.string "reference"
    t.string "claimed_at"
    t.string "returned_at"
    t.string "sub_purpose"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "claim_time"
    t.datetime "return_time"
    t.string "remarks"
    t.string "academicPurpose"
    t.text "additionalRemarks"
    t.string "course_and_section"
    t.index ["user_id"], name: "index_reservations_on_user_id"
  end

  create_table "specifications", force: :cascade do |t|
    t.string "field"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "facility_id"
    t.integer "item_id"
    t.index ["facility_id"], name: "index_specifications_on_facility_id"
    t.index ["item_id"], name: "index_specifications_on_item_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "firstName"
    t.string "lastName"
    t.string "idNumber"
    t.string "course"
    t.string "section"
    t.integer "role", default: 0
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "block", default: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
