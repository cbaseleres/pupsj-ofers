Rails.application.routes.draw do

  get 'errors/not_found'
  get 'errors/internal_server_error'
  devise_for :users
  resources :facilities
  resources :items, :path => 'equipment'
  resources :organizations
  resources :reservations
  resources :field_of_studies
  resources :professors
  resources :courses
  devise_scope :user do
  root to: "devise/sessions#new", :as => '/'
  end

  get '/users' => 'pages#users', :as => "users"
  get '/student/dashboard' => 'pages#dashboard'
  get '/student/reservation' => 'reservations#studentReservation'
  get '/student/reservation/:id' => 'reservations#student_view', :as => "view_reservation"
  post '/student/reservation/:id/cancel' => 'reservations#stud_cancel', :as => "student_cancel_reservation"
  get '/student/calendar' => 'pages#calendar'
  get '/student/messages' => 'pages#messages'
  get '/student/messages/:id' => 'pages#show', :as => "message_show"
  get '/admin' => 'pages#index'
  get '/admin_login' => 'pages#admin_login'
  post '/authenticate_admin' => 'pages#authenticate_admin'
  get '/admin/add_user' => 'pages#admin_add_user'
  post '/admin_create_user' => 'pages#admin_create_user'
  post '/student/see_reservations' => 'reservations#see_reservations'
  get '/student/item_list' => 'pages#item_list', :as => "item_list"
  get '/student/facility_list' => 'pages#facility_list', :as => "facility_list"
  get '/data' => 'reservations#data'
  get '/equip' => 'items#export_equipment'
  get '/equip_res' => 'items#export_equipment_pdf'  
  get '/org' => 'organizations#export_organization'
  get '/org_csv' => 'organizations#export_equipment'
  get '/prof' => 'professors#export_equipment'
  get '/prof_csv' => 'professors#export_equipment_csv'
  get '/fac' => 'facilities#export_equipment'
  get '/facs' => 'facilities#export_data'
  get '/fac_pdf' => 'facilities#export_facility_pdf'
  get '/res' => 'reservations#export_equipment'
  get '/cors' => 'courses#export_courses'
  get '/cors_csv' => 'courses#export_equipment'
  get '/res_equips' => 'items#export_equip_res'
  get '/fields' => 'field_of_studies#export_fields_pdf', :as => 'fields_export'
  get '/fields_csv' => 'field_of_studies#export_equipment'
  get '/reservation_today' => 'pages#export_reservation_today'
  get '/cancel_reservation/:id' => 'reservations#cancel', :as => 'cancel_reservation'
  post '/cancel/:id' => 'reservations#push_cancel', :as => 'submit_cancelation'
  get '/organization/new/non_acad' => 'organizations#nonAcadOrg', :as => 'non_acad'
  post '/organization/new/non_acad' => 'organizations#createNonAcadOrg', :as => 'create_non_acad'
  get '/purposes' => 'reservations#purposeForm'
  # get '/update_reservation/:id' => 'reservations#claims', as: => 'claim_time'
  post '/update_reservation/:id' => 'reservations#claims', :as => 'claim_time'
  get '/return/:id' => 'reservations#returnsView', :as => 'return'
  post '/update_reservations/:id' => 'reservations#returns', :as => 'return_time'
  post '/get_item_count' => 'reservations#get_item_count'
  # post '/cancel/:id' => 'reservations#stud_cancel', :as 'stud_cancel'
  # get '/list' => 'reservations#list' as => "list"
  # Edit non acad orgs
  get '/organization/update/:id' => 'organizations#editNonAcadOrg', :as => 'editNonAcad'
  patch '/organization/update/:id' => 'organizations#updateNonAcadOrg', :as => 'updateNonAcad'
  post '/unblock/:id' => 'pages#unblockUser', :as => 'unblock_user'
  post '/block/:id' => 'pages#blockUser', :as => "block_user"
  get "/reservation_pdf" => "reservations#reservation_pdf", as: "reservation_pdf"

  get '/show_item/:id' => "pages#item_show", as: "item_show"

  get '/facility_item/:id' => "pages#facility_show", as: "facility_show"


  match "/404", :to => "errors#not_found", :via => :all
  match "/500", :to => "errors#internal_server_error", :via => :all

  get "*any", via: :all, to: "errors#not_found"


  # root to: 'devise/session#new'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
