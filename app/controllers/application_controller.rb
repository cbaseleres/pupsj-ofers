class ApplicationController < ActionController::Base
  
  def after_sign_in_path_for(resource)
  student_dashboard_path
  end

  def after_sign_out_path_for(resource_or_scope)
  new_user_session_path
  end

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:firstName, :lastName,:idNumber, :course, :section, :email])
  end

  def after_sign_in_path_for(resource)
    # generate role based path for user
    resource.admin? ? admin_path : student_dashboard_path
  end
end
