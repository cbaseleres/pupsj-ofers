class FieldOfStudiesController < ApplicationController
  before_action :find_fieldOfStudy, only: [:show, :update, :edit, :destroy]
  before_action :authenticate

  def index
    # @fieldOfStudies = FieldOfStudy.paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
    search = params[:search_trips]
    @fieldOfStudies = if search.blank?
              FieldOfStudy.paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
            else
              FieldOfStudy.search(query: search).paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
            end
  end

  def new
    @fieldOfStudy = FieldOfStudy.new
  end

  def create
    @fieldOfStudy = FieldOfStudy.new(fieldOfStudy_params)
    if @fieldOfStudy.save
      flash[:success] = "Field Of Study Added."
      redirect_to field_of_studies_path
    else
      render 'new'
    end
  end

  def show
  end

  def update
    if @fieldOfStudy.update(fieldOfStudy_params)
      flash[:success] = "Field Of Study Updated."
      redirect_to field_of_study_path
    else
      render 'edit'
    end
  end

  def edit
  end

  def destroy
    @fieldOfStudy.destroy
    flash[:success] = "Field of Study Deleted."
    redirect_to field_of_studies_path
  end

  def test

  end

  def export_fields_pdf
    @fields = FieldOfStudy.all
    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => "Field of Studies Master List", :template => "field_of_studies/fields_pdf"
      end
    end
  end

  def export_equipment
    respond_to do |format|
      format.csv do
        equipments = FieldOfStudy.all
        export_headers("#{Time.now}-Field_of_Studies")
        self.response_body = export_for_equipment(equipments)
      end
    end
  end


  private

  def export_headers(label)
    filename = "#{label}-#{Time.zone.today}"
    response.headers["Cache-Control"] = "no-cache"
    response.headers["X-Accel-Buffering"] = "no"
    response.headers["Content-Type"] ||= "text/csv"
    response.headers["Content-Disposition"] =
      "attachment; filename=#{filename}.csv"
    response.headers["Content-Transfer-Encoding"] = "binary"
  end

  def export_for_equipment(entries)
    header2 = ["Field Of Studies Master List"].to_csv
    header = ["Name"].to_csv
    Enumerator.new do |row|
      entries.each_with_index do |x, y|
        row << header2 if y == 0
        row << header if y == 0
        row << [x.name].to_csv
      end
    end
  end

  def authenticate
    if current_user.nil?
    flash[:danger] = "You are unauthorized!"
    redirect_to new_user_session_path
    elsif current_user.student?
      flash[:danger] = "You are unauthorized!"
      redirect_to student_dashboard_path
    end
  end

  def fieldOfStudy_params
    params.require(:field_of_study).permit(:name)
  end

  def find_fieldOfStudy
    @fieldOfStudy = FieldOfStudy.find(params[:id])
  end
end
