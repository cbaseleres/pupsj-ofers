class ReservationsController < ApplicationController
  before_action :find_reservation, only: [:show, :update, :edit, :destroy, :cancel, :push_cancel, :claims, :returns, :returnsView, :student_view, :stud_cancel]
  protect_from_forgery except: :studentReservation

  def index
    search = params[:search_trips]
    @reservations = if search.blank?
              if params[:status]
                if params[:status] == "all"
                  Reservation.all.paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
                else
                  Reservation.where(status: params[:status]).paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
                end
              else
                Reservation.all.paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
              end
            else
              Reservation.search(q: search).paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
            end
    @reservation_statuses = [
    ["All Reservation", "all"],
    ["Active", "Active"],
    ["Cancelled", "Canceled"],
    ["Claimed", "Claimed"],
    ["Returned", "Done"],
  ]
  end

  def new
    if params[:reservation]
      if params[:reservation][:dateFrom]
        @date_param = params.dig("reservation", "dateFrom")
        date = DateTime.parse(@date_param)
        @reservations = Reservation.where(dateFrom: date..(date + 24.hours))
        @date = date.strftime("%B %e, %Y")
        @has_picked_date = true
      elsif params[:reservation][:purpose]
        @purpose = params.dig("reservation", "purpose")
      elsif params[:reservation][:reservation_records_attributes]
        id = params[:reservation][:reservation_records_attributes].values.last[:item_id]
        quantityCount = Item.find(id.to_i).quantity
      end
    end
    @params_purpose = params[:reservation][:purpose] if params[:reservation]
    @reservation = Reservation.new
  end

  def create
    @reservation = current_user.reservations.new(reservation_params)
    item_ids = []
    facility_ids = []
    errors = []
    if current_user.admin?
      error_path = new_reservation_path
      success_path = reservations_path
    else
      error_path = student_reservation_path
      success_path = student_dashboard_path
    end
    errors << "Please choose a equipment or facility to reserve" if @reservation.reservation_records.empty?
    @reservation.reservation_records.each do |x|
      item_ids << x.item_id if x.item_id
      facility_ids << x.facility_id if x.facility_id
    end

    if item_ids.uniq.length != item_ids.length || facility_ids.uniq.length != facility_ids.length
      errors << "DUPLICATE ITEM OR FACILITY"
    end
    date = @reservation.dateFrom
    @timeFrom = @reservation.timeFrom
    @timeTo = @reservation.timeTo
    @reservation.reservation_records.each do |x|
      id = x.facility_id.present? ? x.facility_id : x.item_id
      if x.facility_id.present?
        id = x.facility_id
        obj = Facility.find(id)
      else
        id = x.item_id
        obj = Item.find(id)
      end
      obj.reservation_records.each do |reserved|
        next if reserved.reservation.dateFrom != date
        if reserved.reservation.timeFrom <= @timeTo && reserved.reservation.timeTo <= @timeFrom
          if @reservation.purpose == "SCHOOL EVENT"
            reserved.reservation.update(status: "Cancelled") unless reserved.reservation.status == "SCHOOL EVENT"
            title = "Canceled Reservation"
            message_content = "Your reservation has been cancelled due to a School Event. Please approach the admin to know more."
            @message = Message.new(title: title, content: message_content, user_id: reserved.reservation.user_id, reservation_id: reserved.reservation.id)
            # binding.pry
            @message.save
            
          elsif x.quantity.present? && (reserved.quantity + x.quantity) > obj.quantity
            errors << "The Quantity Count For Your Reservation is invalid"
          elsif x.quantity.blank?
            errors << "Your Item/Facility Reservation is in conflict with existing and active reservations"
          end
        end
      end
    end
    if errors.present?
      flash[:danger] = errors.to_sentence
      return redirect_to error_path
    end
    if @reservation.save
      @reservation.update(status: "Active")
      prof = Professor.find_by(name: @reservation.sub_purpose).id if @reservation.purpose == "ACADEMIC"
      professor = Professor.find(prof)
      if professor.email != ""
        UserMailer.notify_professor(prof, @reservation.id).deliver_now! if @reservation.purpose == "ACADEMIC"
      end
      @reservation.update(academicPurpose: params[:academicPurpose]) if params[:academicPurpose].present?
      @reservation.update(academicPurpose: params[:academicPurposeCustom]) if params[:academicPurposeCustom].present?
      flash[:success] = "Successfully Created Your Reservation"
      set_path = if current_user.admin?
              reservations_path
            else
              student_dashboard_path(reservation: @reservation.id )
            end
      redirect_to set_path
    else
      flash[:danger] = @reservation.errors.full_messages.to_sentence
      redirect_to error_path
      
    end
  end

  def studentReservation
    if params[:reservation]
      if params[:reservation][:dateFrom]
        @date_param = params.dig("reservation", "dateFrom")
        date = DateTime.parse(@date_param)
        @reservations = Reservation.where(dateFrom: date..(date + 24.hours)).take(5)
        @date = date.strftime("%B %e, %Y")
        @has_picked_date = true
      elsif params[:reservation][:purpose]
        @purpose = params.dig("reservation", "purpose")
      elsif params[:reservation][:reservation_records_attributes]
        id = params[:reservation][:reservation_records_attributes].values.last[:item_id]
        quantityCount = Item.find(id.to_i).quantity
      end
    end
    @reservation = Reservation.new
  end

  def purposeForm
    @purpose = params.dig("reservation", "purpose")
  end

  def createReserevation
    @reservation = current_user.reservations.new(reservation_params)
    if @reservation.save
      redirect_to @reservation
    else
      render "new"
    end
  end

  def get_item_count
    item = Item.find(params[:item][:id])
    respond_to do |format|
      format.json { render json: item.quantity }
    end
  end

  def show
  end

  def student_view
  end

  
  def update
    item_ids = []
    facility_ids = []
    errors = []
    @reserved_items = params[:reservation][:reservation_records_attributes]
    @timeFrom = params[:reservation][:timeFrom]
    @timeTo = params[:reservation][:timeTo]
    @purpose = params[:reservation][:purpose]
    date = params[:reservation][:dateFrom]
    errors << "Please choose a equipment or facility to reserve" if @reserved_items.empty?
    @reserved_items.values.each do |x|
      item_ids << x.dig("item_id") if x.dig("item_id")
      facility_ids << x.dig("facility_id") if x.dig("facility_id")
    end
    if item_ids.uniq.length != item_ids.length || facility_ids.uniq.length != facility_ids.length
      errors << "DUPLICATE ITEM OR FACILITY"
    end

    @reserved_items.values.each do |x|
      id = x.dig("facility_id").present? ? x.dig("facility_id") : x.dig("item_id")
      if x.dig("facility_id").present?
        obj = Facility.find(id.to_i)
      else
        obj = Item.find(id.to_i)
      end
      obj.reservation_records.each do |reserved|
        next if reserved.reservation.dateFrom != date
        if reserved.reservation.timeFrom <= @timeTo && reserved.reservation.timeTo <= @timeFrom
          if @purpose == "SCHOOL EVENT"
            reserved.update(status: "CANCELLED") unless reserved.status == "SCHOOL EVENT"
          elsif x.quantity.present? && (reserved.quantity + x.quantity) > obj.quantity
            errors << "The Quantity Count For Your Reservation is invalid"
          elsif x.quantity.blank?
            errors << "Your Item/Facility Reservation is in conflict with existing and active reservations"
          end
        end
      end
    end
    if errors.present?
      flash[:danger] = errors.to_sentence
      return redirect_to error_path
    end
    if @reservation.update(reservation_params)
      @reservation.update(status: "Active")
      @reservation.update(academicPurpose: params[:academicPurpose]) if params[:academicPurpose].present?
      flash[:success] = "Successfully Updated Your Reservation"
      redirect_to reservations_path
    else
      flash[:danger] = @reservation.errors.full_messages.to_sentence
      redirect_to new_reservation_path
    end
  end

  def edit
    if params[:reservation]
      if params[:reservation][:dateFrom]
        @date_param = params.dig("reservation", "dateFrom")
        date = DateTime.parse(@date_param)
        @reservations = Reservation.where(dateFrom: date..(date + 24.hours)).take(5)
        @date = date.strftime("%B %e, %Y")
        @has_picked_date = true
      elsif params[:reservation][:purpose]
        @purpose = params.dig("reservation", "purpose")
      elsif params[:reservation][:reservation_records_attributes]
        id = params[:reservation][:reservation_records_attributes].values.last[:item_id]
        quantityCount = Item.find(id.to_i).quantity
      end
    end
  end

  def destroy
    @reservation.destroy
    redirect_to reservations_path
  end

  def cancel
  end

  def push_cancel
    if current_user.valid_password?(params[:pass])
      @reservation.update(remarks: params[:reason], status: "Cancelled")
      flash[:success] = "Reservation Canceled."
      title = "Canceled Reservation"
      message_content = "Your reservation has been cancelled due to #{params[:reason]}. Please approach the admin to know more."
      @message = Message.new(title: title, content: message_content, user_id: @reservation.user_id, reservation_id: @reservation.id)
      @message.save
    end
    redirect_to reservations_path
  end

  def stud_cancel
    @reservation.update(status: "Cancelled")
    flash[:success] = "Reservation Cancelled."
    redirect_to student_dashboard_path
  end

  # RETRIEVING DATA TO FILL FULLCALENDAR
  def data
    reservation =[]
    
    reservations = Reservation.all()
    reservations.each do |res|
      title = []
      start = res.dateFrom.to_s+"T"+(res.timeFrom.to_s[11..24]).gsub(" UTC", "")
      finish = res.dateFrom.to_s+"T"+(res.timeTo.to_s[11..24]).gsub(" UTC", "")
      from = res.timeFrom.strftime("%I:%M%p")
      to = res.timeTo.strftime("%I:%M%p")
      resDate = res.dateFrom.strftime("%b %e, %Y")
      res.reservation_records.each do |rec|
        if rec.item_id?
          title.push("Equipment: #{rec.item.name} -- Quantity: #{rec.quantity}<br>")
        elsif rec.facility_id?
          title.push("Facility: #{rec.facility.name}<br>  ")
        end
      end
      ress = {
        "start" => start,
        "end" => finish,
        "title" => title,
        "data" => {
          "from" => from,
          "to" => to,
          "date" => resDate
        }
      }
      reservation.push(ress)
    end
    render :json => reservation
  end

 # ADDS CLAIM TIME TO RESERVATION

  def claims
    @reservation.update(claim_time: Time.now, status: "Claimed")
    redirect_to reservations_path
  end

  def returnsView
    @remarks = params["remarks"]
  end

  # ADDS RETURN TIME AND REMARKS FOR THE RESERVATION
  def returns
    if params[:remarks] != "Good Condition"
      user = User.find(@reservation.user_id)
      @reservation.update(status: "Done",
                          return_time: Time.now,
                          remarks: params[:remarks],
                          additionalRemarks: params[:additionalRemarks])
      if params[:blockUser] == "true"
        binding.pry
        user.update(block: true)
        msg = "You have been Block From the PUPSJ-OEFRS due to your reservation on"\
              "#{@reservation.dateFrom} because of #{@reservation.remarks}"
        user.messages.create(title: "Blocked From Reservation System",
                             content: msg,
                             reservation_id: @reservation.id)
      end
    else
      @reservation.update(status: "Done", return_time: Time.now, remarks: params[:remarks])
    end
    redirect_to reservations_path
  end

  def export_equipment
    @from = params[:from]
    @to = params[:to]
    @purpose = params[:purpose]
    @status = params[:status]
    binding.pry
    if @purpose.present? && @status.present?
      @reservations = Reservation.where('dateFrom BETWEEN ? AND ? AND purpose = ? AND status = ?', DateTime.parse(@from), DateTime.parse(@to), @purpose, @status)
    elsif @purpose.present?
      @reservations = Reservation.where('dateFrom BETWEEN ? AND ? AND purpose = ?', DateTime.parse(@from), DateTime.parse(@to), @purpose)
    elsif @status.present?
      @reservations = Reservation.where('dateFrom BETWEEN ? AND ? AND status = ?', DateTime.parse(@from), DateTime.parse(@to), @status)
    else
      @reservations = Reservation.where('dateFrom BETWEEN ? AND ?', DateTime.parse(@from), DateTime.parse(@to)
      )
    end
    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => "Reservation data",:template => "reservations/reservation_data_pdf"
      end
    end
  end

  

  def reservation_pdf
    @reservation = Reservation.find(params[:reservation_id])
    @user = @reservation.user
    respond_to do |format|
      format.pdf do
        render :pdf => "reservation_pdf",:template => "reservations/reservation_pdf"
      end
    end
  end

  def stud_cancel
    @reservation.update(status: "Cancelled")
    redirect_to student_dashboard_path
  end

  private

  def authenticate
    if current_user.nil?
    flash[:danger] = "You are unauthorized!"
    redirect_to new_user_session_path
    elsif current_user.student?
      flash[:danger] = "You are unauthorized!"
      redirect_to student_dashboard_path
    end
  end

  # def export_headers(label)
  #   filename = "#{label}-#{Time.zone.today}"
  #   response.headers["Cache-Control"] = "no-cache"
  #   response.headers["X-Accel-Buffering"] = "no"
  #   response.headers["Content-Type"] ||= "text/csv"
  #   response.headers["Content-Disposition"] =
  #     "attachment; filename=#{filename}.csv"
  #   response.headers["Content-Transfer-Encoding"] = "binary"
  # end

  # def export_for_equipment(entries)
  #   header = ["Reservation", "Date", "Quantity", "Time From", "Time To", "Reserved by"].to_csv
  #   Enumerator.new do |row|
  #     entries.each_with_index do |x, y|
  #       row << header if y == 0
  #       x.reservation_records.each do |record|
  #         reserve = record.facility_id.present? ? record.facility.name : record.item.name
  #         row << [reserve, record.reservation.dateFrom.strftime("%b %e, %Y"), record.quantity,  record.reservation.timeFrom.strftime("%I:%M %p"), record.reservation.timeTo.strftime("%I:%M %p"), "#{x.user&.firstName} #{x.user&.lastName}" ].to_csv
  #       end
  #     end
  #   end
  # end

  def reservation_params
    params.require(:reservation).permit(:equipment, :status,   :facility, :purpose, :dateFrom, :dateTo, :timeFrom, :sub_purpose,
                                        :timeTo, :course_and_section, reservation_records_attributes: [:id, :facility_id, :item_id, :quantity, :_destroy])
  end

  def find_reservation
    @reservation = Reservation.find(params[:id])
  end
end
