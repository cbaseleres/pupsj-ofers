class PagesController < ApplicationController

  def index
    @equipment = Item.all
    @facilities = Facility.all
    @upcoming_reservations = Reservation.upcoming.paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
  end

  def landingPage

  end

  def item_list
    @equipments = Item.all
  end

  def facility_list
    @facilities = Facility.all
  end

  def item_show
    @item = Item.find(params[:id])
    @reservation_records = @item.reservation_records
  end

  def facility_show
    @facility = Facility.find(params[:id])
    @reservation_records = @facility.reservation_records
  end

  def calendar

  end

  def dashboard
    @reservations =  if params[:status]
                       if params[:status] == "all"
                         current_user.reservations.all.paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
                       else
                         current_user.reservations.where(status: params[:status]).paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
                       end
                    else
                     current_user.reservations.paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
                    end
    @reservation_statuses = [
      ["All Reservation", "all"],
      ["Active", "Active"],
      ["Cancelled", "Cancelled"],
      ["Claimed", "Claimed"],
      ["Returned", "Done"],
    ]
    @reservation = Reservation.find(params[:reservation]) if params[:reservation]
  end

  def messages
    @messages = Message.where(user_id: current_user.id).paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
  end

  def show
    @message = Message.find(params[:id])
    @message.update(read: true) if !@message.read?
    @reservation = Reservation.find(@message.reservation_id) if @message.reservation_id?

  end

  def admin_login
  end

  def authenticate_admin
    user = User.find_by(email: params[:Email])
    return render 'admin_login' if user && user.student?
    if user && user.valid_password?(params[:Password])
      sign_in user
      flash[:success] = "Successfully signed in."
      redirect_to admin_path
    else
      render 'admin_login'
    end
  end
  
  #  ADMIN CREATE USER
  def admin_add_user
    @user = User.new
  end

  def admin_create_user
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "User Added."
      redirect_to users_path
    else
      redirect_to  admin_add_user_path
    end
  end

  def users
    search = params[:search_trips]
    @users = if search.blank?
              User.student.paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
            else
              User.search(query: search).paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
            end
  end

  def unblockUser
    @user = User.find(params[:id])
    @user.update(block: false)
    redirect_to users_path
  end

  def blockUser
    @user = User.find(params[:id])
    reason = params[:reason]
    @user.update(block: true)
    admin = current_user.firstName
    @message = Message.new(title: 'Blocked (' + reason + ')', content: 'You have been blocked, please see sir/maam ' + admin + ' immediately.', user_id: @user.id)
    @message.save
    redirect_to users_path
  end

  def export_reservation_today
    @reservations = Reservation.where(dateFrom: Date.today)
    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => "Reservation for #{Date.today}", :template => "pages/res_today_pdf"
      end
    end
  end

  private

  def export_headers(label)
    filename = "#{label}-#{Time.zone.today}"
    response.headers["Cache-Control"] = "no-cache"
    response.headers["X-Accel-Buffering"] = "no"
    response.headers["Content-Type"] ||= "text/csv"
    response.headers["Content-Disposition"] =
      "attachment; filename=#{filename}.csv"
    response.headers["Content-Transfer-Encoding"] = "binary"
  end

  def export_for_equipment(entries)
    header = ["Reservation", "Date", "Quantity", "Time From", "Time To", "Reserved by"].to_csv
    Enumerator.new do |row|
      entries.each_with_index do |x, y|
        row << header if y == 0
        x.reservation_records.each do |record|
          reserve = record.facility_id.present? ? record.facility.name : record.item.name
          row << [reserve, record.reservation.dateFrom.strftime("%b %e, %Y"), record.quantity,  record.reservation.timeFrom.strftime("%I:%M %p"), record.reservation.timeTo.strftime("%I:%M %p"), "#{x.user&.firstName} #{x.user&.lastName}" ].to_csv
        end
      end
    end
  end

  def ensure_admin
    path = if current_user.present?
             student_dashboard_path
           else
             root_path
           end
    redirect_to path, alert: "UNAUTHORIZED ACCESS" unless current_user.admin?
  end

  def user_params
    params.require(:user).permit(:email, :firstName, :lastName, :idNumber, :course, :section, :role, :password, :password_confirmation)
  end
end