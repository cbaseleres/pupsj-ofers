class FacilitiesController < ApplicationController
  before_action :find_facility, only: [:show, :update, :edit, :destroy]
  before_action :authenticate

  def index
    # @facilities = Facility.paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
    search = params[:search_trips]
    @facilities = if search.blank?
              Facility.paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
            else
              Facility.search(query: search).paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
            end
  end

  def new
    @facility = Facility.new
  end

  def create
    @facility = Facility.new(facility_params)
    if @facility.save
      flash[:success] = "Facility Added."
      redirect_to facilities_path
    else
      render 'new'
    end
  end

  def show
  end

  def update
    if @facility.update(facility_params)
      flash[:success] = "Facility Updated."
      redirect_to facilities_path
    else
      render 'edit'
    end
  end

  def edit
    if params[:facility]
      if params[:facility][:specifications_attributes]
      end
    end
  end

  def destroy
    @facility.destroy
    flash[:success] = "Facility Deleted."
    redirect_to facilities_path
  end


  def export_equipment
    respond_to do |format|
      format.csv do
        equipments = Facility.all
        export_headers("#{Time.zone.now}-Facility")
        self.response_body = export_for_equipment(equipments)
      end
    end
  end

  def export_facility_pdf
    @from = params[:from]
    @to = params[:to]
    @reservations = Reservation.where('dateFrom BETWEEN ? AND ?', DateTime.parse(@from), DateTime.parse(@to))
    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => "Facilities reserved", :template => "facilities/fac_data_pdf"
      end
    end
  end

  def export_data
    @facilities= Facility.all
    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => "Facilities",:template => "facilities/fac_data"
      end
    end
  end


  private

  def authenticate
    if current_user.nil?
    flash[:danger] = "You are unauthorized!"
    redirect_to new_user_session_path
    elsif current_user.student?
      flash[:danger] = "You are unauthorized!"
      redirect_to student_dashboard_path
    end
  end

  def export_headers(label)
    filename = "#{label}-#{Time.zone.today}"
    response.headers["Cache-Control"] = "no-cache"
    response.headers["X-Accel-Buffering"] = "no"
    response.headers["Content-Type"] ||= "text/csv"
    response.headers["Content-Disposition"] =
      "attachment; filename=#{filename}.csv"
    response.headers["Content-Transfer-Encoding"] = "binary"
  end

  def export_for_equipment(entries)
    header2 = ["Facility Master List"]
    header = ["Facility Name", "Description" , "Status"].to_csv
    Enumerator.new do |row|
      entries.each_with_index do |x, y|
        row << header2 if y == 0
        row << header if y == 0
        row << [x.name, x.description, x.status].to_csv
      end
    end
  end
  def facility_params
    params.require(:facility).permit(:name, :status, specifications_attributes: [:id, :field, :content, :_destroy])
  end

  def find_facility
    @facility = Facility.find(params[:id])
  end

end
