class ItemsController < ApplicationController
  before_action :find_item, only: [:show, :update, :edit, :destroy]
  before_action :authenticate

  def index
    # @items = Item.paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
    search = params[:search_trips]
    @items = if search.blank?
              Item.paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
            else
              Item.search(query: search).paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
            end
  end

  def new
    @item = Item.new
  end

  def create
    @item = Item.new(item_params)

    if @item.save
      # binding.pry
      flash[:success] = "Equipment Added."
      redirect_to items_path
    else
      render 'new'
    end
  end

  def show
  end

  def update
    if @item.update(item_params)
      flash[:success] = "Equipment Updated."
      redirect_to items_path
    else
      render 'edit'
    end
  end

  def edit
    if params[:item]
      if params[:item][:specifications_attributes]
      end
    end
  end

  def destroy
    @item.destroy
    flash[:success] = "Equipment Deleted."
    redirect_to items_path
  end

  def test

  end

  def export_equipment
    respond_to do |format|
      format.csv do
        equipments = Item.all
        export_headers("#{Time.zone.now}-Equipments")
        self.response_body = export_for_equipment(equipments)
      end
    end
  end

  def export_equipment_pdf
    @items= Item.all
    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => "Equipments",:template => "items/equip_data"
      end
    end
  end

  def export_equip_res
    @from = params[:from]
    @to = params[:to]
    @reservations = Reservation.where('dateFrom BETWEEN ? AND ?', DateTime.parse(@from), DateTime.parse(@to))
    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => "Reservation data",:template => "items/equipment_res"
      end
    end
  end

  private

  def authenticate
    if current_user.nil?
    flash[:danger] = "You are unauthorized!"
    redirect_to new_user_session_path
    elsif current_user.student?
      flash[:danger] = "You are unauthorized!"
      redirect_to student_dashboard_path
    end
  end

  def export_headers(label)
    filename = "#{label}-#{Time.zone.today}"
    response.headers["Cache-Control"] = "no-cache"
    response.headers["X-Accel-Buffering"] = "no"
    response.headers["Content-Type"] ||= "text/csv"
    response.headers["Content-Disposition"] =
      "attachment; filename=#{filename}.csv"
    response.headers["Content-Transfer-Encoding"] = "binary"
  end

  def export_for_equipment(entries)
    header2 = ["Equipment Master List"].to_csv
    header = ["Equipment Name", "Quantity"].to_csv
    Enumerator.new do |row|
      entries.each_with_index do |x, y|
        row << header2 if y == 0
        row << header if y == 0
        row << [x.name, x.quantity].to_csv
      end
    end
  end

  def item_params
    params.require(:item).permit(:name, :quantity, specifications_attributes: [:id, :field, :content, :_destroy])
  end

  def find_item
    @item = Item.find(params[:id])
  end
end
