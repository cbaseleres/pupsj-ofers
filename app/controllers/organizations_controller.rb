class OrganizationsController < ApplicationController
  before_action :find_organization, only: [:show, :update, :edit, :destroy, :editNonAcadOrg]
  before_action :authenticate

  def index
    # @organizations = Organization.paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
    search = params[:search_trips]
    @organizations = if search.blank?
                Organization.paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
              else
                Organization.search(query: search).paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
              end
  end

  def new
    @organization = Organization.new
  end

  def create
    @organization = Organization.new(organization_params)
    if params.dig("organization","course").present? && @organization.save
      flash[:success] = "Organization Added."
      redirect_to organizations_path
    else
      render 'new'
    end
  end

  def nonAcadOrg
    @organization = Organization.new
  end

  def createNonAcadOrg
    @organization = Organization.new(name: params.dig("organization", "name"))
    if @organization.save
      flash[:success] = "Organization Added."
      redirect_to organizations_path
    end
  end

  def editNonAcadOrg
  end

  def updateNonAcadOrg
    if @organization.update(name: params.dig("organization", "name"))
      flash[:success] = "Organization Updated."
      redirect_to organizations_path
    else
      render 'edit'
    end
  end

  def show
  end

  def update
    if @organization.update(organization_params)
      flash[:success] = "Organization Updated."
      redirect_to organizations_path
    else
      render 'edit'
    end
  end

  def edit
  end

  def destroy
    @organization.destroy
    flash[:success] = "Organization Deleted."
    redirect_to organizations_path
  end

  def test

  end

  def export_organization
    @organizations= Organization.all
    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => "Organizations",:template => "organizations/org_data"
      end
    end
  end

  def export_equipment
    respond_to do |format|
      format.csv do
        equipments = Organization.all
        export_headers("#{Time.now}-Organization")
        self.response_body = export_for_equipment(equipments)
      end
    end
  end

  private

  def authenticate
    if current_user.nil?
    flash[:danger] = "You are unauthorized!"
    redirect_to new_user_session_path
    elsif current_user.student?
      flash[:danger] = "You are unauthorized!"
      redirect_to student_dashboard_path
    end
  end

  def export_headers(label)
    filename = "#{label}-#{Time.zone.today}"
    response.headers["Cache-Control"] = "no-cache"
    response.headers["X-Accel-Buffering"] = "no"
    response.headers["Content-Type"] ||= "text/csv"
    response.headers["Content-Disposition"] =
      "attachment; filename=#{filename}.csv"
    response.headers["Content-Transfer-Encoding"] = "binary"
  end

  def export_for_equipment(entries)
    header2 = ["Organization Master List"].to_csv
    header = ["Organization Name", "Course"].to_csv
    Enumerator.new do |row|
      entries.each_with_index do |x, y|
        row << header2 if y == 0
        row << header if y == 0
        org_course = x.course.present? ? Course.find(x.course.to_i).name : "N/A"
        row << [x.name, org_course ].to_csv
      end
    end
  end

  def organization_params
    params.require(:organization).permit(:name, :course, :facultyInCharge)
  end

  def find_organization
    @organization = Organization.find(params[:id])
  end
end
