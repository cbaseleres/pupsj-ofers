class CoursesController < ApplicationController
  before_action :find_course, only: [:show, :update, :edit, :destroy]
  before_action :authenticate

  def index
    # @courses = Course.paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
    search = params[:search_trips]
    @courses = if search.blank?
              Course.paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
            else
              Course.search(query: search).paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
            end
  end

  def new
    @course = Course.new
  end

  def create
    @course = Course.new(course_params)
    if @course.save
      flash[:success] = "Course Added."
      redirect_to courses_path
    else
      render 'new'
    end
  end

  def show
  end

  def update
    if @course.update(course_params)
      flash[:success] = "Course Updated."
      redirect_to courses_path
    else
      render 'edit'
    end
  end

  def edit
  end

  def destroy
    @course.destroy
    flash[:success] = "Course Deleted."
    redirect_to courses_path
  end

  def test

  end

  def export_courses
    @courses= Course.all
    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => "Courses",:template => "courses/course_data"
      end
    end
  end

  def export_equipment
    respond_to do |format|
      format.csv do
        equipments = Course.all
        export_headers("#{Time.now}-Courses")
        self.response_body = export_for_equipment(equipments)
      end
    end
  end


  private

  def export_headers(label)
    filename = "#{label}-#{Time.zone.today}"
    response.headers["Cache-Control"] = "no-cache"
    response.headers["X-Accel-Buffering"] = "no"
    response.headers["Content-Type"] ||= "text/csv"
    response.headers["Content-Disposition"] =
      "attachment; filename=#{filename}.csv"
    response.headers["Content-Transfer-Encoding"] = "binary"
  end

  def export_for_equipment(entries)
    header2 = ["Course Master List"].to_csv
    header = ["Name"].to_csv
    Enumerator.new do |row|
      entries.each_with_index do |x, y|
        row << header2 if y == 0
        row << header if y == 0
        row << [x.name].to_csv
      end
    end
  end

  def authenticate
    if current_user.nil?
    flash[:danger] = "You are unauthorized!"
    redirect_to new_user_session_path
    elsif current_user.student?
      flash[:danger] = "You are unauthorized!"
      redirect_to student_dashboard_path
    end
  end

  def course_params
    params.require(:course).permit(:name)
  end

  def find_course
    @course = Course.find(params[:id])
  end
end
