class ProfessorsController < ApplicationController
  before_action :find_professor, only: [:show, :update, :edit, :destroy]
  before_action :authenticate

  def index
    # @professors = Professor.paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
    search = params[:search_trips]
    @professors = if search.blank?
                Professor.paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
              else
                 Professor.search(query: search).paginate(:page => params[:page], :per_page => 7).order("created_at DESC")
              end
  end

  def new
    @professor = Professor.new
  end

  def create
    @professor = Professor.new(professor_params)
    if @professor.save  
      binding.pry
      flash[:success] = "Professor Added."
      redirect_to professors_path
    else
      render 'new'
    end
  end

  def show
  end

  def update
    if @professor.update(professor_params)
      flash[:success] = "Professor Updated."
      redirect_to professors_path
    else
      render 'edit'
    end
  end


  def edit
  end

  def destroy
    @professor.destroy
    flash[:success] = "professor Deleted."
    redirect_to professors_path
  end

  def test

  end

  def export_equipment
    @professors= Professor.all
    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => "Professors",:template => "professors/prof_data"
      end
    end
  end

  def export_equipment_csv
    respond_to do |format|
      format.csv do
        equipments = Professor.all
        export_headers("#{Time.now}-Professors")
        self.response_body = export_for_equipment(equipments)
      end
    end
  end

  private

  def authenticate
    if current_user.nil?
    flash[:danger] = "You are unauthorized!"
    redirect_to new_user_session_path
    elsif current_user.student?
      flash[:danger] = "You are unauthorized!"
      redirect_to student_dashboard_path
    end
  end

  def export_headers(label)
    filename = "#{label}-#{Time.zone.today}"
    response.headers["Cache-Control"] = "no-cache"
    response.headers["X-Accel-Buffering"] = "no"
    response.headers["Content-Type"] ||= "text/csv"
    response.headers["Content-Disposition"] =
      "attachment; filename=#{filename}.csv"
    response.headers["Content-Transfer-Encoding"] = "binary"
  end

  def export_for_equipment(entries)
    header2 = ["Professors Master List"].to_csv
    header = ["Professor Name", "Field of Study"].to_csv
    Enumerator.new do |row|
      entries.each_with_index do |x, y|
        row << header2 if y == 0
        row << header if y == 0
        row << [x.name, FieldOfStudy.find(x.fieldOfStudy.to_i).name].to_csv
      end
    end
  end

  def professor_params
    params.require(:professor).permit(:name, :fieldOfStudy, :email)
  end

  def find_professor
    @professor = Professor.find(params[:id])
  end
end
