class Course < ApplicationRecord
  validates :name, presence: true, uniqueness: true

  def self.search(query: nil)
    query = "%#{query}%"
    where("name LIKE ?", query).order(:name)
  end
end
