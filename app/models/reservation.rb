class Reservation < ApplicationRecord
  PURPOSE = %w[ACADEMIC ORGANIZATIONAL]
  ADMIN_PURPOSE = ["ACADEMIC", "ORGANIZATIONAL", "SCHOOL EVENT"]

  belongs_to :user
  has_many :reservation_records, dependent: :destroy
  accepts_nested_attributes_for :reservation_records, :allow_destroy => true
  validates :dateFrom, presence: true
  validates :timeFrom, presence: true
  validates :timeTo, presence: true
  validates :purpose, presence: true
#  validates :sub_purpose, presence: true

  scope :upcoming, ->() { where('dateFrom >= ?', Time.zone.now) }
  scope :for_today , ->() { where('dateFrom >= ?', Time.now) }

  before_validation do
    self.errors.add(:base, "Invalid Time Range For Reservation.") if self.timeTo < self.timeFrom || self.timeFrom == self.timeTo
  end

  before_save do
    self.reference = Reservation.generate_ref_code
  end

  def self.generate_ref_code
    loop do
      random_code = SecureRandom.hex(3)
      shuffle_code = "PUPSJ-OEFRS-" + random_code.chars.shuffle!.join('')
      break shuffle_code unless self.exists?(reference: shuffle_code)
    end
  end

  def self.search(q: )
   joins(:user).where("firstName LIKE :keyword OR lastName LIKE :keyword", keyword: "%#{q}%").order(created_at: :desc)
  end


  def self.active
    self.where(status: "Active")
  end

  def self.claimed
    self.where(status: "Claimed")
  end

  def self.cancelled
    self.where(status: "Cancelled")
  end

  def self.returned
    self.where(status: "Done")
  end
end
