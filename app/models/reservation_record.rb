class ReservationRecord < ApplicationRecord
  belongs_to :reservation
  belongs_to :facility, optional: true
  belongs_to :item, optional: true
end
