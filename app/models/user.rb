class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :authentication_keys => {idNumber: true, login: false}

  validates :firstName, presence: true
  validates :lastName, presence: true
  validates :idNumber, presence: true, uniqueness: true
  has_many :reservations, dependent: :destroy
  enum role: { student: 0, admin: 1 }
  attr_writer :login

  def login
    @login || self.idNumber || self.email
  end


  has_many :messages

  before_save do
    if self.role == 'student'
      validates_presence_of :course
      validates_presence_of :section
    end
  end


  def self.search(query: nil)
    where("firstName LIKE :query or lastName LIKE :query", query: "%#{query}%").order(:idNumber)
  end

end
