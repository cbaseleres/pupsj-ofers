class Organization < ApplicationRecord
  validates :name, uniqueness: { case_sensitive: false }, presence: true
  # validates :course, presence: true

  def self.search(query: nil)
    query = "%#{query}%"
    where("name LIKE ?", query).order(:name)
  end
end
