class Item < ApplicationRecord
  # belongs_to :reservation
  validates :name, presence: true, uniqueness: true
  validates :quantity, presence: true
  has_many :reservation_records, dependent: :destroy

  has_many :specifications, dependent: :destroy
  accepts_nested_attributes_for :specifications, :allow_destroy => true


  def self.search(query: nil)
    query = "%#{query}%"
    where("name LIKE ?", query).order(:name)
  end
end
