class Specification < ApplicationRecord
  belongs_to :facility, optional: true
  belongs_to :item, optional: true
end
