json.array!(Reservation.all) do |event|
  json.extract! event, :id, :purpose, :dateFrom, :timeFrom, :timeTo
  json.url Rails.application.routes.url_helpers.data_path(event, format: :json)
end
