class UserMailer < ApplicationMailer
  default from: "pupsj.ors@gmail.com"

  # def notify_professor(reservation)
  #   # @professor = prof
  #   @reservation = Reservation.find(reservation)
  #   email = "Test@gmail.com"
  #   mail(to: email, subject: "PUPSJ RESERVATION NOTIFICATION")
  # end

  def notify_professor(prof, reservation)
    @professor = Professor.find(prof)
    @reservation = Reservation.find(reservation)
    email = @professor.email
    mail(to: email, subject: "PUPSJ RESERVATION NOTIFICATION")
  end
end
