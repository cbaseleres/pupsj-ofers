$('#froala-editor').froalaEditor();


function checkMaxQuantity(e) {
  var number = e
  $.ajax({
      type: 'POST',
      url: '/get_item_count',
      data: {item: { id: number }},
      success: function(data) {
        console.log(data);
        $('#maxQuant').html("MAX QUANTITY: " + data);
      }
  });
}

function closeModal() {
  $('#memberModal').addClass('hide');
  $("#memberModal").attr("style", "")
}

$('#calendar').fullCalendar({
  header: {
    left: 'prev,next today',
    center: 'title',
    right: 'month,agendaWeek,agendaDay'
  },

  eventLimit: true,
  eventColor: '#800000',
  eventTextColor: '#ffffff',
  events: {
    url: '/data.json'
  },
  eventMouseover: function(calEvent, jsEvent) {

    var tooltip = '<div class="tooltipevent p-2" style="border-radius: 10px;padding: 2px; width:auto;height:auto;background:maroon;color:#ffffff;position:absolute;z-index:10001;">Reservation: ' + calEvent.title + '<br>Time: ' + calEvent.data.from + '-' + calEvent.data.to + '</div>';
    var $tooltip = $(tooltip).appendTo('body');

    $(this).mouseover(function(e) {
      $(this).css('z-index', 10000);
      $(this).css('cursor', 'pointer');
      $tooltip.fadeIn('500');
      $tooltip.fadeTo('10', 1.9);
    }).mousemove(function(e) {
      $tooltip.css('top', e.pageY + 10);
      $tooltip.css('left', e.pageX + 20);
    });
  },
  eventMouseout: function(calEvent, jsEvent) {
    $(this).css('z-index', 8);
    $('.tooltipevent').remove();
  },
  eventClick: function(calEvent, jsEvent, view, resourceObj) {
  $("#successModal").modal("show");
  $(".modal-header").html("<h5 class='modal-title'>Reservation for "+calEvent.data.date+"</h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>");
  $(".modal-body").html("<p><b>Reservation:</b> "+calEvent.title+"<br><b>Time: </b>"+calEvent.data.from+" - "+calEvent.data.to+"</p")
},

});
// for feather icons
feather.replace()

// for date picker
function formatDate(date) {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [year, month, day].join('-');
}
var d = new Date();
d.setDate(d.getDate() + 3);
// console.log(dd);
$('#pickDate').datepicker({
  startDate: d,
  todayHighlight: true,
  format: {
    toDisplay: function(date, format, language) {
      var dx = new Date(date);
      dx.setDate(dx.getDate());
      return formatDate(dx);
    },
    toValue: function(date, format, language) {
      var dx = new Date(date);
      dx.setDate(dx.getDate());
      return formatDate(dx);
    }
  }
});

$('#datePick').datepicker({
  startDate: d,
  todayHighlight: true,
  format: {
    toDisplay: function(date, format, language) {
      var dx = new Date(date);
      dx.setDate(dx.getDate());
      return formatDate(dx);
    },
    toValue: function(date, format, language) {
      var dx = new Date(date);
      dx.setDate(dx.getDate());
      return formatDate(dx);
    }
  }
});
  $('#closeModal').on('click', function(){
    $('#memberModal').addClass('hide');
  });

function checkSample() {
  console.log("rerer");
  alert("GUMAGANA");
  var date = this.value;
  $.ajax({
      type: 'POST',
      url: "<%= Rails.application.routes.url_helpers.student_see_reservations_path %>",
      data: date,
      success: function() {
        alert(date);
      }
  });
};

$('#firstDate').datepicker({
        todayHighlight: true,
        format: {
          toDisplay: function (date, format, language) {
              var dx = new Date(date);
              dx.setDate(dx.getDate());
              return formatDate(dx);
          },
          toValue: function (date, format, language) {
              var dx = new Date(date);
              dx.setDate(dx.getDate());
              return formatDate(dx);
          }
        }

      });
      $('#secondDate').datepicker({
        todayHighlight: true,
        format: {
          toDisplay: function (date, format, language) {
              var dx = new Date(date);
              dx.setDate(dx.getDate());
              return formatDate(dx);
          },
          toValue: function (date, format, language) {
              var dx = new Date(date);
              dx.setDate(dx.getDate());
              return formatDate(dx);
          }
        }

      });
